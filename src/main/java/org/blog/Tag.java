package org.blog;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Tag extends PanacheEntity {
    public String label;

    @ManyToMany(mappedBy = "tags")
    private List<Post> posts = new ArrayList<>();

    public List<Post> getPosts() {
        return posts;
    }
}
