package org.blog;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

@Path("/tags")
public class TagResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        List<Tag> tags = Tag.listAll();
        return Response.ok(tags).build();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getById(@PathParam("id") Long id) {
        return Tag.findByIdOptional(id)
                .map(tag -> Response.ok(tag).build())
                .orElse(Response.status(Response.Status.NOT_FOUND).build());
    }

    @POST
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(Tag tag) {
        Tag.persist(tag);
        if (tag.isPersistent()) {
            return Response.created(URI.create("/tags" + tag.id)).build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }

    @PUT
    @Path("/{id}")
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") Long id, Tag tag) {
        Tag entity = Tag.findById(id);
        if(entity == null) {
            throw new NotFoundException();
        }

        entity.label = tag.label;

        return Response.ok(entity).build();
    }

    @DELETE
    @Transactional
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteById(@PathParam("id") Long id) {
        boolean deleted = Tag.deleteById(id);
        if (deleted) {
            throw new NotFoundException();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }
}